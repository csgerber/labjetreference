package com.zeeroapps.sunflower.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.data.PlantAndGardenPlantings
import com.zeeroapps.sunflower.databinding.ListItemGardenPlantingBinding
import com.zeeroapps.sunflower.view_models.PlantAndGardenPlantingsViewModel

class GardenPlantAdapter(var plantingsList: MutableList<PlantAndGardenPlantings>) : RecyclerView.Adapter<GardenPlantAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val binding: ListItemGardenPlantingBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.list_item_garden_planting, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(plantingsList[position])
    }

    override fun getItemCount(): Int {
        return plantingsList.size
    }

    fun updateList(plantings: List<PlantAndGardenPlantings>) {
        val diffCallback = GardenPlantDiffCallback(plantingsList, plantings)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        plantingsList.clear()
        plantingsList.addAll(plantings)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(private val binding: ListItemGardenPlantingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(plantAndGardenPlantings: PlantAndGardenPlantings) {
            binding.viewModel = PlantAndGardenPlantingsViewModel(
                    binding.root.context, plantAndGardenPlantings)
            binding.executePendingBindings()
        }
    }
}