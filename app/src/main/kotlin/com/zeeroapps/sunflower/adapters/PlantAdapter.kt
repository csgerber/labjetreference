package com.zeeroapps.sunflower.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.data.Plant
import com.zeeroapps.sunflower.databinding.ListItemPlantBinding
import com.zeeroapps.sunflower.ui.PlantListFragmentDirections

class PlantAdapter(private val plantList: MutableList<Plant>) : RecyclerView.Adapter<PlantAdapter.ViewHolder>() {
    private var layoutInflater: LayoutInflater? = null
    override fun onCreateViewHolder(parent: ViewGroup, i: Int): ViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding: ListItemPlantBinding = DataBindingUtil.inflate(layoutInflater!!, R.layout.list_item_plant, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plant = plantList[position]
        holder.bind(plant, createClickListener(plant.plantId))
    }

    private fun createClickListener(plantId: String?): View.OnClickListener {
        return View.OnClickListener { view ->
            val direction = PlantListFragmentDirections.actionPlantListFragmentToPlantDetailFragment(plantId!!)
            Navigation.findNavController(view).navigate(direction)
        }
    }

    override fun getItemCount(): Int {
        return plantList.size
    }

    fun updateItems(plants: List<Plant>?) {
        val diffCallback = PlantDiffCallback(plantList, plants)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        plantList.clear()
        plants?.let { plantList.addAll(it) }
        diffResult.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(var binding: ListItemPlantBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(plant: Plant?, clickListener: View.OnClickListener?) {
            binding.clickListener = clickListener
            binding.plant = plant
            binding.executePendingBindings()
        }
    }
}