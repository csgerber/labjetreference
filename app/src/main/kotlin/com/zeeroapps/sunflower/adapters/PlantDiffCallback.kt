package com.zeeroapps.sunflower.adapters

import androidx.recyclerview.widget.DiffUtil
import com.zeeroapps.sunflower.data.Plant

class PlantDiffCallback(private val oldList: List<Plant>, private val newList: List<Plant>?) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList!!.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldPlant = oldList[oldItemPosition]
        val newPlant = newList?.get(newItemPosition)
        return oldPlant.plantId === newPlant?.plantId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldPlant = oldList[oldItemPosition]
        val newPlant = newList?.get(newItemPosition)
        return oldPlant == newPlant
    }
}