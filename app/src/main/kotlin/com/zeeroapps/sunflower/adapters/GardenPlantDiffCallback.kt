package com.zeeroapps.sunflower.adapters

import androidx.recyclerview.widget.DiffUtil
import com.zeeroapps.sunflower.data.PlantAndGardenPlantings

class GardenPlantDiffCallback(private val oldList: List<PlantAndGardenPlantings>, private val newList: List<PlantAndGardenPlantings>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldPlant = oldList[oldItemPosition]
        val newPlant = newList[newItemPosition]
        return oldPlant.plant?.plantId === newPlant.plant?.plantId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldPlant = oldList[oldItemPosition]
        val newPlant = newList[newItemPosition]
        return oldPlant == newPlant
    }
}