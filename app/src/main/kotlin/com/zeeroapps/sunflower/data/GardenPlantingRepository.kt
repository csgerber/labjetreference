package com.zeeroapps.sunflower.data

import android.os.AsyncTask
import androidx.lifecycle.LiveData

class GardenPlantingRepository(var gardenPlantingDao: GardenPlantingDao) {
    fun createGardenPlanting(plantId: String) {
        AsyncTask.execute {
            val gardenPlanting = GardenPlanting(plantId)
            gardenPlantingDao.insertGardenPlanting(gardenPlanting)
        }
    }

    fun getGardenPlantingForPlant(plantId: String?): LiveData<GardenPlanting> {
        return gardenPlantingDao.getGardenPlantingForPlant(plantId)
    }

    val gardenPlants: LiveData<List<GardenPlanting>>
        get() = gardenPlantingDao.gardenPlantings

    //    private val IO_EXECUTOR = Executors.newSingleThreadExecutor()
    val plantAndGardenPlantings: LiveData<List<PlantAndGardenPlantings>>
        get() = gardenPlantingDao.plantAndGardenPlantings

    //
    //    fun runOnIoThread() {
    //        IO_EXECUTOR.execute()
    //    }
    companion object {
        var instance: GardenPlantingRepository? = null
        fun getInstance(gardenPlantingDao: GardenPlantingDao): GardenPlantingRepository? {
            if (instance == null) {
                synchronized(GardenPlanting::class.java) {
                    if (instance == null) {
                        instance = GardenPlantingRepository(gardenPlantingDao)
                    }
                }
            }
            return instance
        }
    }
}