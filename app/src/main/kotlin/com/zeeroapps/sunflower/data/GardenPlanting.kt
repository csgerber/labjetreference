package com.zeeroapps.sunflower.data

import androidx.room.*
import java.util.*

@Entity(tableName = "garden_plantings", foreignKeys = [ForeignKey(entity = Plant::class, parentColumns = ["id"], childColumns = ["plant_id"])], indices = [Index("plant_id")])
class GardenPlanting(@field:ColumnInfo(name = "plant_id") var plantId: String) {
    @kotlin.jvm.JvmField
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var gardenPlantingId: Long = 0

    @kotlin.jvm.JvmField
    @ColumnInfo(name = "plant_date")
    var plantDate = Calendar.getInstance()

    @kotlin.jvm.JvmField
    @ColumnInfo(name = "last_watering_date")
    var lastWateringDate = Calendar.getInstance()
}