package com.zeeroapps.sunflower.data

import androidx.room.Embedded
import androidx.room.Relation
import java.util.*

class PlantAndGardenPlantings {
    @JvmField
    @Embedded
    var plant: Plant? = null

    @JvmField
    @Relation(parentColumn = "id", entityColumn = "plant_id")
    var gardenPlantings: List<GardenPlanting> = ArrayList()
}