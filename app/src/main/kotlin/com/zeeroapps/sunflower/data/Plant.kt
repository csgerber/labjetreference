package com.zeeroapps.sunflower.data

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide

@Entity(tableName = "plants")
class Plant {
    @PrimaryKey
    @ColumnInfo(name = "id")
    var plantId: String = ""

    var name: String? = null
    var description: String? = null
    var growZoneNumber = 0
    var wateringInterval = 7
    var imageUrl = ""

    companion object {
        @JvmStatic
        @BindingAdapter("imageFromUrl")
        fun imageFromUrl(view: ImageView, imageUrl: String?) {
            if (imageUrl != null && imageUrl.isNotEmpty()) Glide.with(view.context).load(imageUrl).into(view)
        }
    }
}