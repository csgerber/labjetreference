package com.zeeroapps.sunflower.data

import androidx.lifecycle.LiveData

class PlantRepository internal constructor(private val plantDao: PlantDao) {
    val plants: LiveData<List<Plant>>
        get() = plantDao.plants

    fun getPlant(id: String?): LiveData<Plant> {
        return plantDao.getPlant(id.orEmpty())
    }

    fun getPlantsByGrowZoneNumber(growZoneNo: Int): LiveData<List<Plant>> {
        return plantDao.getPlantsByGrowZoneNumber(growZoneNo)
    }

    companion object {
        @Volatile
        private var instance: PlantRepository? = null
        fun getInstance(plantDao: PlantDao): PlantRepository? {
            if (instance == null) {
                synchronized(PlantRepository::class.java) { if (instance == null) instance = PlantRepository(plantDao) }
            }
            return instance
        }
    }
}