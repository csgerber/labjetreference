package com.zeeroapps.sunflower.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.zeeroapps.sunflower.workers.SeedDatabaseWorker

@Database(entities = [Plant::class, GardenPlanting::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun plantDao(): PlantDao
    abstract fun gardenPlantingDao(): GardenPlantingDao

    companion object {
        private var instance: AppDatabase? = null
        fun getInstance(context: Context?): AppDatabase? {
            if (instance == null) {
                synchronized(AppDatabase::class.java) { if (instance == null) instance = buildDatabase(context) }
            }
            return instance
        }

        private fun buildDatabase(context: Context?): AppDatabase {
            return Room.databaseBuilder(context!!, AppDatabase::class.java, "sunflower-db")
                    .addCallback(object : Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            val workRequest = OneTimeWorkRequest.Builder(SeedDatabaseWorker::class.java).build()
                            WorkManager.getInstance().enqueue(workRequest)
                        }
                    }).build()
        }

        fun destroyDatabase() {
            instance = null
        }
    }
}