package com.zeeroapps.sunflower.utils

import android.content.Context
import com.zeeroapps.sunflower.data.AppDatabase
import com.zeeroapps.sunflower.data.GardenPlantingRepository
import com.zeeroapps.sunflower.data.PlantRepository
import com.zeeroapps.sunflower.view_models.GardenPlantingListViewModelFactory
import com.zeeroapps.sunflower.view_models.PlantDetailViewModelFactory
import com.zeeroapps.sunflower.view_models.PlantListViewModelFactory

object InjectorUtils {
    private fun getPlantRepository(context: Context?): PlantRepository {
        return PlantRepository.getInstance(AppDatabase.getInstance(context)!!.plantDao())!!
    }

    private fun getGardenPlantingRepository(context: Context?): GardenPlantingRepository {
        return GardenPlantingRepository.getInstance(AppDatabase.getInstance(context)!!.gardenPlantingDao())!!
    }

    fun providePlantListViewModelFactory(context: Context?): PlantListViewModelFactory {
        val repository = getPlantRepository(context)
        return PlantListViewModelFactory(repository)
    }

    fun provideGardenPlantListViewModelFactory(context: Context?): GardenPlantingListViewModelFactory {
        val gardenPlantingRepository = getGardenPlantingRepository(context)
        return GardenPlantingListViewModelFactory(gardenPlantingRepository)
    }

    fun providePlantDetailViewModelFactory(context: Context?, plantId: String): PlantDetailViewModelFactory {
        return PlantDetailViewModelFactory(getPlantRepository(context), getGardenPlantingRepository(context), plantId)
    }
}