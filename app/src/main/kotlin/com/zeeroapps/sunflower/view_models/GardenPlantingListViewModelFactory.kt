package com.zeeroapps.sunflower.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import com.zeeroapps.sunflower.data.GardenPlantingRepository

class GardenPlantingListViewModelFactory(var gardenPlantingRepository: GardenPlantingRepository) : NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GardenPlantingListViewModel(gardenPlantingRepository) as T
    }
}