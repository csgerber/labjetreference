package com.zeeroapps.sunflower.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.zeeroapps.sunflower.data.GardenPlanting
import com.zeeroapps.sunflower.data.GardenPlantingRepository
import com.zeeroapps.sunflower.data.PlantAndGardenPlantings
import java.util.*

class GardenPlantingListViewModel(gardenPlantingRepository: GardenPlantingRepository) : ViewModel() {
    var plantAndGardenPlantings: LiveData<List<PlantAndGardenPlantings>>
    var gardenPlantings: LiveData<List<GardenPlanting>> = gardenPlantingRepository.gardenPlants

    init {
        plantAndGardenPlantings = Transformations.map(gardenPlantingRepository.plantAndGardenPlantings) { plantings: List<PlantAndGardenPlantings> ->
            val plantingsListNew: MutableList<PlantAndGardenPlantings?> = ArrayList()

            for (i in plantings.indices) {
                if (plantings[i].gardenPlantings.isNotEmpty()) {
                    plantingsListNew.add(plantings[i])
                }
            }

            return@map plantingsListNew.filterNotNull()
        }
    }
}