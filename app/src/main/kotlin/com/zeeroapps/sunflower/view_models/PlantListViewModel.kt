package com.zeeroapps.sunflower.view_models

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.zeeroapps.sunflower.data.Plant
import com.zeeroapps.sunflower.data.PlantRepository

class PlantListViewModel internal constructor(private val plantRepository: PlantRepository?) : ViewModel() {
    private val NO_GROW_ZONE = -1
    private val growZoneNumber = MutableLiveData<Int>()

    val plants = MediatorLiveData<List<Plant>>()

    fun setGrowZoneNumber(no: Int) {
        growZoneNumber.value = no
    }

    fun clearGrowZoneNumber() {
        growZoneNumber.value = NO_GROW_ZONE
    }

    val isFiltered: Boolean
        get() = growZoneNumber.value != NO_GROW_ZONE

    init {
        growZoneNumber.value = NO_GROW_ZONE
        val livePlantList = Transformations.switchMap(growZoneNumber) { gz_no: Int ->
            if (gz_no == NO_GROW_ZONE) {
                return@switchMap plantRepository?.plants
            } else {
                return@switchMap plantRepository?.getPlantsByGrowZoneNumber(gz_no)
            }
        }

        plants.addSource(livePlantList) { plants: List<Plant> -> this.plants.value = plants }
    }
}