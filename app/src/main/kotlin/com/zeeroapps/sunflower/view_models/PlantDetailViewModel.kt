package com.zeeroapps.sunflower.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.zeeroapps.sunflower.data.GardenPlanting
import com.zeeroapps.sunflower.data.GardenPlantingRepository
import com.zeeroapps.sunflower.data.Plant
import com.zeeroapps.sunflower.data.PlantRepository

class PlantDetailViewModel(
        private val plantRepository: PlantRepository,
        private val gardenPlantingRepository: GardenPlantingRepository,
        var plantId: String
) : ViewModel() {
    private val gardenPlantingForPlant: LiveData<GardenPlanting> = gardenPlantingRepository.getGardenPlantingForPlant(plantId)

    var isPlanted: LiveData<Boolean>? = null
    var plant: LiveData<Plant> = plantRepository.getPlant(plantId)

    fun addPlantToGarden() {
        gardenPlantingRepository.createGardenPlanting(plantId)
    }
}