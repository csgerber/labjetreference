package com.zeeroapps.sunflower.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import com.zeeroapps.sunflower.data.PlantRepository

class PlantListViewModelFactory(private val plantRepository: PlantRepository?) : NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val plantListViewModel = PlantListViewModel(plantRepository)
        return plantListViewModel as T
    }
}