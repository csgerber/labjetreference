package com.zeeroapps.sunflower.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import com.zeeroapps.sunflower.data.GardenPlantingRepository
import com.zeeroapps.sunflower.data.PlantRepository

class PlantDetailViewModelFactory(
        private val plantRepository: PlantRepository,
        private val gardenPlantingRepository: GardenPlantingRepository,
        private val plantId: String
) : NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PlantDetailViewModel(plantRepository, gardenPlantingRepository, plantId) as T
    }
}