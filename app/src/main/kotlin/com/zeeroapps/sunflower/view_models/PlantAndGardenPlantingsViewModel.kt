package com.zeeroapps.sunflower.view_models

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.data.GardenPlanting
import com.zeeroapps.sunflower.data.Plant
import com.zeeroapps.sunflower.data.PlantAndGardenPlantings
import java.text.SimpleDateFormat
import java.util.*

class PlantAndGardenPlantingsViewModel(context: Context, plantings: PlantAndGardenPlantings) : ViewModel() {
    val imageUrl: ObservableField<String?>
    val plantDate: ObservableField<String>
    val waterDate: ObservableField<String>
    var plant: Plant?
    var gardenPlanting: GardenPlanting?

    init {
        plant = plantings.plant
        gardenPlanting = plantings.gardenPlantings!![0]
        val dateFormat = SimpleDateFormat("MMM d, yyyy", Locale.US)
        val plantDateStr = dateFormat.format(gardenPlanting!!.plantDate.time)
        val waterDateStr = dateFormat.format(gardenPlanting!!.lastWateringDate.time)
        val wateringPrefix = context.getString(R.string.watering_next_prefix, waterDateStr)
        val wateringSuffix = context.resources.getQuantityString(R.plurals.watering_next_suffix,
                plant?.wateringInterval ?: 0, plant?.wateringInterval ?: 0)
        imageUrl = ObservableField(plant?.imageUrl.orEmpty())
        plantDate = ObservableField(
                context.getString(R.string.planted_date, plant?.name.orEmpty(), plantDateStr))
        waterDate = ObservableField("$wateringPrefix - $wateringSuffix")
        //Todo: fix waterDate field
    }
}