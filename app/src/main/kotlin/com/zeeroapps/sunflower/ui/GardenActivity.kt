package com.zeeroapps.sunflower.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.databinding.ActivityGardenBinding

class GardenActivity : AppCompatActivity() {
    private var mDrawerLayout: DrawerLayout? = null
    private var mNavController: NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityGardenBinding = DataBindingUtil.setContentView(this, R.layout.activity_garden)
        mDrawerLayout = binding.drawerLayout
        setSupportActionBar(binding.toolbar)
        mNavController = Navigation.findNavController(this, R.id.garden_nav_fragment)
        NavigationUI.setupActionBarWithNavController(this, mNavController!!, mDrawerLayout)
        NavigationUI.setupWithNavController(binding.navigationView, mNavController!!)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(mNavController!!, mDrawerLayout)
    }

    override fun onBackPressed() {
        if (mDrawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}