package com.zeeroapps.sunflower.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.adapters.PlantAdapter
import com.zeeroapps.sunflower.data.Plant
import com.zeeroapps.sunflower.utils.InjectorUtils
import com.zeeroapps.sunflower.view_models.PlantListViewModel
import java.util.*

class PlantListFragment : Fragment() {
    private val plantsList: MutableList<Plant> = ArrayList()
    private var viewModel: PlantListViewModel? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_plant_list, container, false)
        val factory = InjectorUtils.providePlantListViewModelFactory(activity)
        viewModel = ViewModelProviders.of(this, factory).get(PlantListViewModel::class.java)
        val adapter = PlantAdapter(plantsList)
        val recyclerView: RecyclerView = view.findViewById(R.id.plant_list)
        recyclerView.adapter = adapter
        subscribeUI(adapter)
        setHasOptionsMenu(true)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_plant_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    private fun subscribeUI(adapter: PlantAdapter) {
        viewModel?.plants?.observe(viewLifecycleOwner, { plants -> adapter.updateItems(plants) })
    }
}