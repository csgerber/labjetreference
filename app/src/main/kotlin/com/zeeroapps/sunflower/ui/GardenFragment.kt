package com.zeeroapps.sunflower.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.adapters.GardenPlantAdapter
import com.zeeroapps.sunflower.data.PlantAndGardenPlantings
import com.zeeroapps.sunflower.utils.InjectorUtils
import com.zeeroapps.sunflower.view_models.GardenPlantingListViewModel
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class GardenFragment : Fragment() {
    var plantingsList: MutableList<PlantAndGardenPlantings> = ArrayList()
    var mRecyclerView: RecyclerView? = null
    var mTextViewEmptyGarden: TextView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_garden, container, false)
        val adapter = GardenPlantAdapter(plantingsList)
        mTextViewEmptyGarden = view.findViewById(R.id.empty_garden)
        mRecyclerView = view.findViewById(R.id.garden_list)
        mRecyclerView?.adapter = adapter
        subscribeUI(adapter)
        return view
    }

    private fun subscribeUI(adapter: GardenPlantAdapter) {
        val factory = InjectorUtils.provideGardenPlantListViewModelFactory(context)
        val viewModel = ViewModelProviders.of(this, factory).get(GardenPlantingListViewModel::class.java)
        viewModel.gardenPlantings?.observe(viewLifecycleOwner, { gardenPlantings ->
            if (gardenPlantings != null && !gardenPlantings.isEmpty()) {
                mRecyclerView?.visibility = View.VISIBLE
                mTextViewEmptyGarden?.visibility = View.GONE
            } else {
                mRecyclerView?.visibility = View.GONE
                mTextViewEmptyGarden?.visibility = View.VISIBLE
            }
        })
        viewModel.plantAndGardenPlantings.observe(viewLifecycleOwner, { gardenPlantingsList ->
            if (gardenPlantingsList != null && gardenPlantingsList.isNotEmpty()) {
                adapter.updateList(gardenPlantingsList.filterNotNull())
            }
        })
    }
}