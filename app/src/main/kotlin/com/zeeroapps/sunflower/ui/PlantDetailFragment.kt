package com.zeeroapps.sunflower.ui

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.zeeroapps.sunflower.R
import com.zeeroapps.sunflower.databinding.FragmentPlantDetailsBinding
import com.zeeroapps.sunflower.utils.InjectorUtils
import com.zeeroapps.sunflower.view_models.PlantDetailViewModel

/**
 * A simple [Fragment] subclass.
 */
class PlantDetailFragment : Fragment() {
    var shareText: String? = null
    var mViewModel: PlantDetailViewModel? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentPlantDetailsBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_plant_details, container, false)
        val plantId = arguments?.let { PlantDetailFragmentArgs.fromBundle(it).plantId }
        setupViewModel(plantId.orEmpty())
        binding.viewModel = mViewModel
        binding.lifecycleOwner = this
        binding.fab.setOnClickListener { view ->
            mViewModel!!.addPlantToGarden()
            Snackbar.make(view, R.string.added_plant_to_garden, Snackbar.LENGTH_LONG).show()

            //Todo: Hide button after adding plant to garden.
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun setupViewModel(plantId: String) {
        val factory = InjectorUtils.providePlantDetailViewModelFactory(activity, plantId)
        mViewModel = ViewModelProviders.of(this, factory).get(PlantDetailViewModel::class.java)
        mViewModel?.plant?.observe(viewLifecycleOwner, { plant ->
            Log.e(TAG, "onChanged: " + plant?.name)
            shareText = if (plant == null) "" else String.format(getString(R.string.share_text_plant), plant.name)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_plant_detail, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "PlantDetailFragment"
        private const val ARG_ITEM_ITEM = "item_id"
        fun newInstance(plantId: String?): PlantDetailFragment {
            val args = Bundle()
            args.putString(ARG_ITEM_ITEM, plantId)
            val fragment = PlantDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }
}