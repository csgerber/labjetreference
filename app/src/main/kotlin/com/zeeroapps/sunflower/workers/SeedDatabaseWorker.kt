package com.zeeroapps.sunflower.workers

import android.content.Context
import androidx.work.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zeeroapps.sunflower.data.AppDatabase
import com.zeeroapps.sunflower.data.Plant
import java.nio.charset.Charset

class SeedDatabaseWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    override fun doWork(): Result {
        return try {
            val inputStream = applicationContext.assets.open("plants.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            val json = String(buffer, Charset.forName("UTF-8"))
            val plantList = Gson().fromJson<List<Plant>>(json, object : TypeToken<List<Plant?>?>() {}.type)
            val appDatabase: AppDatabase? = AppDatabase.getInstance(applicationContext)
            appDatabase?.plantDao()?.insertAll(plantList)
            Result.success()
        } catch (e: Exception) {
            e.printStackTrace()
            Result.failure()
        }
    }
}